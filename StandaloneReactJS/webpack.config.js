const path = require("path");
const webpack = require('webpack');
const allpages = require('./htmlpages.json').pages;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const hash = Math.random().toString(36).substr(2, 20);
const scriptfolder = "assets/script/rjs";

var htmlpages = [];
for (let i in allpages) {
  allpages[i].map($s => {
    htmlpages.push($s);
  });
}

const getAllHTMLFiles = ($pages) => {
  return $pages.map((page) => {
    return new HTMLWebpackPlugin({
      template: "./src/template.ejs",
      filename: page.page + ".html",
      title: page.title,
      chunks: [page.page],
      hashstring: hash,
      scriptfolder: scriptfolder
    });
  });
}

const getAllEntries = ($pages) => {
  let entries = {};
  $pages.forEach((page) => {
    entries[page.page] = "./src/pages/" + page.page + ".tsx";
  });
  return entries;
}      

let webpackConfig = {
  mode: "development",
  entry: getAllEntries(htmlpages),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: scriptfolder + "/[name]-" + hash + ".js",
    publicPath: ""
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json", ".xml", ".txt"]
  },
  module: {
    rules: [
      { test: /\.scss$/, use: [ "style-loader", "css-loader", "postcss-loader", "sass-loader"] },
      { test: /\.tsx?$/, loader: "ts-loader", options: { configFile: "tsconfig.prod.json" } },
      { test: /\.css$/, use: ["style-loader", "css-loader"] },
      { test: /\.xml$/, use: "raw-loader" },
      { test: /\.txt$/, use: "raw-loader" }
    ],
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './src/assets', to: 'assets' }
    ]),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale/,/(en-gb|de|es|fr|id|ja|ko|nl|pt|ru|zh-cn|zh-tw|th|it)\.js/,), // added in case we need moments, we only need these locales
    ...getAllHTMLFiles(htmlpages)
  ],
  devServer: {
    watchOptions: {
      ignored: [
        path.resolve(__dirname, '__tests__')
      ]
    }
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        vendor: {
          name: "libs",
          reuseExistingChunk: true,
          test: /[\\/]node_modules[\\/](promise-polyfill|axios|react|react-dom|moment)[\\/]/,
          priority: 11,
          enforce: true,
          minChunks: 1,
          chunks: 'all',
        }
        // break header and footer into their own chunks
        ,
        header: {
          name: "header",
          reuseExistingChunk: true,
          test: /[\\/]shared[\\/]MainNavigation[\\/]/,
          priority: 11,
          enforce: true,
          minChunks: 1,
          chunks: 'all',
        },
        footer: {
          name: "footer",
          reuseExistingChunk: true,
          test: /[\\/]shared[\\/]Footer[\\/]/,
          priority: 11,
          enforce: true,
          minChunks: 1,
          chunks: 'all',
        }
      }
    }
  },
  performance: {
    hints: false
  }
};



module.exports = env => {
  switch (env.NODE_ENV) {

    case "production":
      webpackConfig.mode = "production";
      webpackConfig.output = {
        path: path.resolve(__dirname, "dist"),
        filename: scriptfolder + "/[name]-" + hash + ".js",
        publicPath: ""
      };
      const terserOptions = {
        terserOptions: {
          output: {
            comments: false
          }
        },
        parallel: 4,
      }
      webpackConfig.optimization.minimizer = [new TerserPlugin(terserOptions)];
      webpackConfig.plugins.unshift(new CleanWebpackPlugin(['dist']));
      webpackConfig.plugins.push(new webpack.IgnorePlugin(/(pageData).json/));      
      break;

    case "staging":
      webpackConfig.plugins.unshift(new CleanWebpackPlugin(['dist']));
      webpackConfig.plugins.push(new webpack.IgnorePlugin(/(pageData).json/));
      break;
    case "development":
      break;
  }
  webpackConfig.plugins.push(new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify(env.NODE_ENV)}}));

  return webpackConfig;
};
