var path = require("path");
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  mode: 'development',
  entry: ["./src/app.ts"],
  output: {
    path: path.resolve(__dirname, "dist/js"),
    filename: "vuetest.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json", ".vue"]
  },
  module: {
    rules: [
      //{ test: /\.scss$/, use: [ "vue-style-loader", "style-loader", "css-loader", "postcss-loader", "sass-loader"] },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules|vue\/src/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      { 
        test: /\.vue$/, 
        loader: 'vue-loader', 
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          }
        // other vue-loader options go here
        } 
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }/*,
      { test: /\.css$/, use: ["vue-style-loader", "style-loader", "css-loader"] },*/
    ],
  },
  plugins: [
    new VueLoaderPlugin()
  ]
};


