var path = require("path");
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
  mode: 'development',
  entry: ["./src/app.ts"],
  output: {
    path: path.resolve(__dirname, "dist/js"),
    filename: "vuetest.js"
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json", ".vue"]
  },
  module: {
    rules: [
      { test: /\.scss$/, use: [ "vue-style-loader", "style-loader", "css-loader", "postcss-loader", "sass-loader"] },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules|vue\/src/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      { test: /\.vue$/, loader: 'vue-loader' },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      { test: /\.css$/, use: ["vue-style-loader", "style-loader", "css-loader"] }
    ],
  },
  devServer: {
    watchOptions: {
      ignored: [
        path.resolve(__dirname, 'test')
      ]
    }
  },
  plugins: [
    new VueLoaderPlugin()
  ]
};
