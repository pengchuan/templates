import Vue from 'vue';
import App from './App.vue';

export const vueapp = new Vue({
    el: '#vueapp',
    render: h => h(App)
});