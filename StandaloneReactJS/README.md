**Standalone ReactJS project template**

This template contains the basic stuff you will need to get a simple Typescript ReactJS library project up and running using webpack as a bundler.
It will compiled all your source code into a single .js file which consists of your library and the ReactJS framework, so there's no need to link to ReactJS in the container HTML file. This will run on vanilla web servers without the need for a running node server.

The sample code in the repo should suffice to get you up to speed on how to link everything together.

Supports

1) Typescript 3 - ts-loader

2) ReactJS - Typescript 3 support

3) JQuery - Typescript 3 support

4) SCSS, CSS - works in tandem with postcss and browserslist to autofill with vendor specific prefixes to CSS attributes

5) SVG - react-svg-loader


---

##Webpack-dev-server

npm run start - A hot reloading dev server is configured to run on 0.0.0.0:8080 (accessible from machine IP).

---

##Compilation

npm run build - This will compile your code for development usage.

npm run production - This will produce a minified .js for production use.

---

Feel free to take and use this template for your own projects. Let me know if it has helped you or if there's anything useful you think can be added into the template. 

Thanks!